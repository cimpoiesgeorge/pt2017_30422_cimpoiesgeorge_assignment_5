import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {
    final static String newLine = System.getProperty("line.separator");

    public static void main(String[] args) throws URISyntaxException, IOException {
        new Controller().run();
    }

    public void run() throws IOException, URISyntaxException {
        URI uri = new File("data/Activities.txt").toURI();
        List<String> lines = Files.readAllLines(Paths.get(uri), Charset.defaultCharset());
        List<MonitoredData> dataList = lines
                .stream()
                .skip(2)
                .map(line -> new MonitoredData(line.trim()))
                .collect(Collectors.toList());

        System.out.println("Distinct dates: " + countDistinctDays(dataList));
        activityCount(dataList);
        activityCountPerDay(dataList);
        computeTotalActivityiesDurations(dataList);
        findShortActivities(dataList);
    }

    public int countDistinctDays(List<MonitoredData> data) {
        Set<LocalDateTime> result = Stream.concat(
                data.stream().map(activity -> activity.startTime),
                data.stream().map(activity -> activity.endTime))
                .map(date -> date.withHour(0).withMinute(0).withSecond(0))
                .collect(Collectors.toSet());
        return result.size();
    }

    public void activityCount(List<MonitoredData> data) throws IOException {
        Map<String, Long> labelToCount = data.stream()
                .map(activity -> activity.activityLabel)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        String fileContent = "Count\t\tActivity" + newLine + "---------------------" + newLine +
                labelToCount
                        .entrySet()
                        .stream()
                        .map(entry -> entry.getValue() + "\t\t" + entry.getKey())
                        .collect(Collectors.joining(newLine));
        Files.write(Paths.get("data/ActivityCount.txt"), fileContent.getBytes());
    }

    public void activityCountPerDay(List<MonitoredData> data) throws IOException {
        Map<LocalDateTime, List<MonitoredData>> dayMap = data.stream().collect(Collectors.groupingBy(activity -> activity.startTime.withHour(0).withMinute(0).withSecond(0), Collectors.toList()));
        LocalDateTime firstDay = data.stream().map(activity -> activity.startTime).sorted().findFirst().get().withHour(0).withMinute(0).withSecond(0);
        Map<Long, Map<String, Long>> dayToLabelToCount = new HashMap<>();

        for (Map.Entry<LocalDateTime, List<MonitoredData>> dayMapEntry : dayMap.entrySet()) {
            LocalDateTime currentDay = dayMapEntry.getKey();
            List<MonitoredData> activities = dayMapEntry.getValue();
            Map<String, Long> labelToCount = activities.stream()
                    .map(activity -> activity.activityLabel)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            dayToLabelToCount.put(Duration.between(firstDay, currentDay).toDays(), labelToCount);
        }

        String fileContents = dayToLabelToCount
                .entrySet()
                .stream()
                .map(dayToActivityToCountEntry -> new AbstractMap.SimpleEntry<Long, List<String>>(
                        dayToActivityToCountEntry.getKey(),
                        dayToActivityToCountEntry.getValue().entrySet().stream().map(entry -> entry.getKey() + " " + entry.getValue()).collect(Collectors.toList())))
                .map(dayToActivityAndCountListEntry -> dayToActivityAndCountListEntry.getKey() + ": " + dayToActivityAndCountListEntry.getValue())
                .collect(Collectors.joining(newLine));
        Files.write(Paths.get("data/DayStatistics.txt"), fileContents.getBytes());
    }

    public void computeTotalActivityiesDurations(List<MonitoredData> data) throws IOException {
        Map<String, Duration> labelToDuration = data.stream().collect(
                Collectors.toMap(
                        activity -> activity.getActivityLabel(),
                        activity -> activity.duration(),
                        (duration1, duration2) -> duration1.plus(duration2)
                ));
        String fileContent = "Duration\t\tActivity" + newLine + "---------------------" + newLine +
                labelToDuration
                        .entrySet()
                        .stream()
                        .filter(activityToDuration -> activityToDuration.getValue().toHours() >= 10)
                        .map(entry -> entry.getValue() + "\t\t" + entry.getKey())
                        .collect(Collectors.joining(newLine));
        Files.write(Paths.get("data/ActivityDurations.txt"), fileContent.getBytes());
    }

    public void findShortActivities(List<MonitoredData> data) throws IOException {
        Map<String, List<Duration>> labelToDuration = data.stream().collect(
                Collectors.toMap(
                        activity -> activity.getActivityLabel(),
                        activity -> Collections.singletonList(activity.duration()),
                        (durations1, durations2) -> Stream.concat(durations1.stream(), durations2.stream()).collect(Collectors.toList())
                ));
        String fileContent = "Activity" + newLine + "---------------------" + newLine +
                labelToDuration
                        .entrySet()
                        .stream()
                        .map(activityToDurations -> new AbstractMap.SimpleEntry<String, Double>(activityToDurations.getKey(), durationsPercentLessThan(activityToDurations.getValue(), 5)))
                        .filter(activityToPercentUnder5 -> activityToPercentUnder5.getValue() >= 0.9d)
                        .map(activityToPercentUnder5 -> activityToPercentUnder5.getKey() + "\t" + activityToPercentUnder5.getValue())
                        .collect(Collectors.joining(newLine));
        Files.write(Paths.get("data/90PercentUnder5.txt"), fileContent.getBytes());
    }

    public static double durationsPercentLessThan(List<Duration> durations, long minutes) {
        long underXMinutesCount = durations.stream().filter(d -> d.toMinutes() <= minutes).count();
        long totalCount = durations.size();
        return underXMinutesCount / (double) totalCount;
    }
}

