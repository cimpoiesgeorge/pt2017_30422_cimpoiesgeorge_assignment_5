import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class MonitoredData {

    LocalDateTime startTime;
    LocalDateTime endTime;
    String activityLabel;

    private static String FIELD_SEPARATOR = "\t\t";

    public MonitoredData(String inputLine) {
        String[] parts = inputLine.split(FIELD_SEPARATOR, 3);
        if (parts.length != 3) throw new IllegalArgumentException("Invalid line: '" + inputLine + "'");
        this.startTime = parseDate(parts[0]);
        this.endTime = parseDate(parts[1]);
        this.activityLabel = parts[2];
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public Duration duration() {
        return Duration.between(startTime, endTime);
    }

    public static LocalDateTime parseDate(String input) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(input, formatter);
        return dateTime;
    }
}
